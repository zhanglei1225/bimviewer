package com.shenzhong.bimviewer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shenzhong.bimviewer.bean.SMS;
import org.springframework.stereotype.Repository;

@Repository
public interface SMSMapper extends BaseMapper<SMS> {

}
