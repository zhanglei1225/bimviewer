package com.shenzhong.bimviewer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shenzhong.bimviewer.bean.DiskUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface DiskUserMapper extends BaseMapper<DiskUser> {
    @Select("select user_id,username,phone,password,project_name,role from disk_user where username=#{userName} or phone=#{userName}")
    DiskUser getUser(String userName);

    @Select("select count(1) from mini_user where phone=#{phoneNum}")
    int isPhoneExists(String phoneNum);
}
