package com.shenzhong.bimviewer.mapper;

import com.shenzhong.bimviewer.bean.MiniUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface MiniUserMapper {
    @Select("select user_id,username,phone,password,project_name,role from mini_user where username=#{userName} or phone=#{userName}")
    MiniUser getUser(String userName);

    @Select("select count(1) from mini_user where phone=#{phoneNum}")
    int isPhoneExists(String phoneNum);
}
