package com.shenzhong.bimviewer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shenzhong.bimviewer.bean.DiskCalendar;
import org.springframework.stereotype.Repository;

@Repository
public interface DiskCalendarMapper extends BaseMapper<DiskCalendar> {

}
