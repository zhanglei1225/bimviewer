package com.shenzhong.bimviewer.mapper;

import com.shenzhong.bimviewer.bean.MyBim;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface MyBimMapper {
    @Select("select id,author,file_id,integrate_id from my_bim where id=#{id}")
    MyBim getBimById(String id);
}
