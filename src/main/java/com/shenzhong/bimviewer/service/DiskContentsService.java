package com.shenzhong.bimviewer.service;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shenzhong.bimviewer.bean.DiskContents;
import com.shenzhong.bimviewer.bean.DiskUser;
import com.shenzhong.bimviewer.mapper.DiskContentsMapper;
import com.shenzhong.bimviewer.mapper.DiskUserMapper;
import com.shenzhong.bimviewer.pojo.BatchGenerateLinkRequest;
import com.shenzhong.bimviewer.util.PageUtils;
import com.shenzhong.bimviewer.util.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiskContentsService extends ServiceImpl<DiskContentsMapper, DiskContents> {
    private static final Logger logger = LoggerFactory.getLogger(DiskContentsService.class);

    @Autowired
    DiskUserMapper diskUserMapper;

    public DiskUser getUser(String userName) {
        return diskUserMapper.getUser(userName);
    }

    public int isPhoneExists(String phoneNum) {
        return diskUserMapper.isPhoneExists(phoneNum);
    }


    public PageUtils queryPage(Map<String, Object> params) {
        Integer type = Integer.parseInt((String)params.get("type"));
        String description = (String)params.get("description");
        String partName = (String)params.get("partName");
        String createUser = (String)params.get("createUser");
        String uploadDate = (String)params.get("uploadDate");
        Date beginTime = null;
        Date endTime = null;
        if (StringUtils.isNotBlank(uploadDate)) {
            try {
                int addDay = 1;
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                // 查询月数据
                if (uploadDate.length() == 7) {
                    uploadDate = uploadDate + "-01";
                    addDay = DateUtil.getLastDayOfMonth(df.parse(uploadDate));
                }
                beginTime = df.parse(uploadDate);
                //Java日期提前几天(日期加)
                Calendar cal = Calendar.getInstance();
                cal.setTime(beginTime);
                cal.add(Calendar.DAY_OF_MONTH, addDay);//加1天
                endTime = cal.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        IPage<DiskContents> page = this.page(
                new Query<DiskContents>().getPage(params),
                new QueryWrapper<DiskContents>()
                        .like(StringUtils.isNotBlank(description),"description", description)
                        .eq("type", type)
                        .eq(StringUtils.isNotBlank(partName),"part_name", partName)
                        .eq(StringUtils.isNotBlank(createUser),"create_user", createUser)
                        .ge(beginTime != null, "create_time", beginTime)
                        .lt(endTime != null, "create_time", endTime)
                        .orderByDesc("create_time")
        );

        return new PageUtils(page);
    }

    public List<DiskContents> getCreatorList(int type) {
        QueryWrapper<DiskContents> wrapper = new QueryWrapper<DiskContents>().eq("type", type);
        return list(wrapper);
    }

    public List<String> getPathList(BatchGenerateLinkRequest batchGenerateLinkRequest) {
        List<String> uploadDates = batchGenerateLinkRequest.getUploadDate();
        List<DiskContents> list = new ArrayList<>();

        for (String uploadDate : uploadDates) {
            String type = batchGenerateLinkRequest.getType();
            Date beginTime = null;
            Date endTime = null;
            try {
                int addDay = 1;
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                // 查询月数据
                if (uploadDate.length() == 7) {
                    uploadDate = uploadDate + "-01";
                    addDay = DateUtil.getLastDayOfMonth(df.parse(uploadDate));
                }
                beginTime = df.parse(uploadDate);
                //Java日期提前几天(日期加)
                Calendar cal = Calendar.getInstance();
                cal.setTime(beginTime);
                cal.add(Calendar.DAY_OF_MONTH, addDay);//加1天
                endTime = cal.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            QueryWrapper<DiskContents> wrapper = new QueryWrapper<DiskContents>().eq(type!=null,"type", type)
                    .ge(beginTime != null, "create_time", beginTime)
                    .lt(endTime != null, "create_time", endTime)
                    .orderByDesc("create_time");
            list.addAll(list(wrapper));
        }
        List<String> paths = list.stream().map(DiskContents::getFileName).collect(Collectors.toList());
        return paths;
    }
}
