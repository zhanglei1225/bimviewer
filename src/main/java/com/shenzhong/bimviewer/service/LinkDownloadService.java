package com.shenzhong.bimviewer.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.shenzhong.bimviewer.pojo.ShortLink;
import com.shenzhong.bimviewer.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.util.EncodingUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * ClassName: LinkDownloadService
 * Package: com.shenzhong.bimviewer.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/11/24 9:13
 * @Version 11
 */
@Slf4j
@Service
public class LinkDownloadService {

    @Resource
    private ShortLinkService shortLinkService;
    private final Set<String> expireKeySet = new HashSet<>();

    @RefererCheck
    @LinkRateLimiter
    public void handlerShortLink(String shortKey) throws IOException {
        // 判断短链是否存在
        ShortLink shortLink = shortLinkService.findByKey(shortKey);
        if (shortLink == null) {
            throw new RuntimeException("此直链不存在或已失效.");
        }
        // 判断短链是否过期
        if (shortLink.getExpireDate() != null) {
            DateTime now = DateUtil.date();
            boolean isExpire = now.isAfter(shortLink.getExpireDate());
            if (isExpire) {
                expireKeySet.add(shortKey);
                throw new RuntimeException("此链接已过期.");
            }
        }

        // 获取实际文件路径，下载并记录日志
        String storageId = shortLink.getStorageId();
        String filePath = shortLink.getUrl();

        HttpServletRequest request = RequestHolder.getRequest();
        HttpServletResponse response = RequestHolder.getResponse();

        String downloadUrl =storageId+filePath;
        // 判断下载链接是否为 m3u8 格式, 如果是则返回 m3u8 内容.
        if (StrUtil.equalsIgnoreCase(FileUtil.extName(filePath), "m3u8")) {
            String textContent = HttpUtil.getTextContent(downloadUrl);
            response.setContentType("application/vnd.apple.mpegurl;charset=utf-8");
            OutputStream outputStream = response.getOutputStream();
            byte[] textContentBytes = EncodingUtils.getBytes(textContent, CharsetUtil.CHARSET_UTF_8.displayName());
            IoUtil.write(outputStream, true, textContentBytes);
            return;
        }

        // 禁止直链被浏览器 302 缓存.
        response.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate, private");
        response.setHeader(HttpHeaders.PRAGMA, "no-cache");
        response.setHeader(HttpHeaders.EXPIRES, "0");

        // 重定向到下载链接.
        String parameterType = request.getParameter("type");
        if (StrUtil.equals(parameterType, "preview")) {
            downloadUrl = UrlUtils.concatQueryParam(downloadUrl, "type", "preview");
        }

        response.sendRedirect(downloadUrl);


    }
}
