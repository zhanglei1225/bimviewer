package com.shenzhong.bimviewer.service;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shenzhong.bimviewer.bean.DiskCalendar;
import com.shenzhong.bimviewer.mapper.DiskCalendarMapper;
import com.shenzhong.bimviewer.mapper.DiskUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiskCalendarService extends ServiceImpl<DiskCalendarMapper, DiskCalendar> {
    private static final Logger logger = LoggerFactory.getLogger(DiskCalendarService.class);

    public boolean isAlreadyUpload() {
        String today = DateUtil.today();
        QueryWrapper<DiskCalendar> wrapper = new QueryWrapper<DiskCalendar>()
                .eq(StringUtils.isNotBlank(today), "upload_date", today);
        return this.count(wrapper) > 0;
    }


    public void saveUploadDate(int type) {
        if (type != 0 || this.isAlreadyUpload()) {
            return;
        }
        saveCalendar(DateUtil.today());
    }

    public void saveCalendar(String uploadDate) {
        DiskCalendar diskCalendar = new DiskCalendar();
        diskCalendar.setUploadDate(uploadDate);
        this.save(diskCalendar);
    }
}
