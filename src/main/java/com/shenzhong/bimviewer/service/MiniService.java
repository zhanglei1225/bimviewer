package com.shenzhong.bimviewer.service;

import com.shenzhong.bimviewer.bean.BridgePart;
import com.shenzhong.bimviewer.bean.MiniUser;
import com.shenzhong.bimviewer.bean.MyBim;
import com.shenzhong.bimviewer.mapper.MiniUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MiniService {
    private static final Logger logger = LoggerFactory.getLogger(MiniService.class);

    @Autowired
    MiniUserMapper miniUserMapper;

    public MiniUser getUser(String userName) {
        return miniUserMapper.getUser(userName);
    }

    public List<BridgePart> getBridgeParts() {
        List<BridgePart> partList = new ArrayList<>(5);
        BridgePart part1 = new BridgePart();
        part1.setId("100001");
        part1.setName("0-1桩");
        part1.setTypeName("桩基");
        part1.setPositionDesc("0");
        part1.setIsFinish(0);
        partList.add(part1);
        BridgePart part2 = new BridgePart();
        part2.setId("100002");
        part2.setName("0-2桩");
        part2.setTypeName("桩基");
        part2.setPositionDesc("0");
        part2.setIsFinish(1);
        partList.add(part2);
        BridgePart part3 = new BridgePart();
        part3.setId("100003");
        part3.setName("0-1承");
        part3.setTypeName("承台");
        part3.setPositionDesc("0");
        part3.setIsFinish(0);
        partList.add(part3);
        BridgePart part4 = new BridgePart();
        part4.setId("100004");
        part4.setName("0-1墩");
        part4.setTypeName("墩身");
        part4.setPositionDesc("0");
        part4.setIsFinish(0);
        partList.add(part4);
        BridgePart part5 = new BridgePart();
        part5.setId("100005");
        part5.setName("0-1桩");
        part5.setTypeName("桩基");
        part5.setPositionDesc("0");
        part5.setIsFinish(0);
        partList.add(part5);
        return partList;
    }

    public int isPhoneExists(String phoneNum) {
        return miniUserMapper.isPhoneExists(phoneNum);
    }
}
