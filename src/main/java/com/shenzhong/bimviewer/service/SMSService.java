package com.shenzhong.bimviewer.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shenzhong.bimviewer.bean.SMS;
import com.shenzhong.bimviewer.mapper.SMSMapper;
import org.springframework.stereotype.Service;

@Service
public class SMSService extends ServiceImpl<SMSMapper, SMS> {

}
