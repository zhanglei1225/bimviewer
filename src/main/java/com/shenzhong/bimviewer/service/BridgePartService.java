package com.shenzhong.bimviewer.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shenzhong.bimviewer.bean.BridgePart;
import com.shenzhong.bimviewer.bean.PartsStatus;
import com.shenzhong.bimviewer.mapper.BridgePartMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BridgePartService extends ServiceImpl<BridgePartMapper, BridgePart> {

    public int isPartIdExists(String partId) {
        QueryWrapper<BridgePart> wrapper = new QueryWrapper<BridgePart>()
                .eq(StrUtil.isNotBlank(partId), "part_id", partId);
        List<BridgePart> partList = this.list(wrapper);
        return partList.size();
    }

    public boolean setFinish(String partId, Integer status) {
        QueryWrapper<BridgePart> wrapper = new QueryWrapper<BridgePart>()
                .eq(StrUtil.isNotBlank(partId), "part_id", partId);
        List<BridgePart> partList = this.list(wrapper);
        BridgePart bridgePart = partList.get(0);
        bridgePart.setIsFinish(status);
        return saveOrUpdate(bridgePart);
    }

    public List<String> getFinishList(String bridgeId) {
        QueryWrapper<BridgePart> wrapper = new QueryWrapper<BridgePart>()
                .eq(StrUtil.isNotBlank(bridgeId), "bridge_id", bridgeId)
                .eq("is_finish", 1);
        List<BridgePart> partList = this.list(wrapper);
        List<String> partIdList = new ArrayList<>(10);
        for (BridgePart bridgePart : partList) {
            partIdList.add(bridgePart.getPartId());
        }
        return partIdList;
    }

    public List<BridgePart> getBridgeInfo(String bridgeId) {
        QueryWrapper<BridgePart> wrapper = new QueryWrapper<BridgePart>()
                .eq(StrUtil.isNotBlank(bridgeId), "bridge_id", bridgeId)
                .orderByAsc("name");
        return this.list(wrapper);
    }

    public PartsStatus getPartsStatus() {
        PartsStatus partsStatus = new PartsStatus();
        List<String> normalFinish = new ArrayList<>(3);
        normalFinish.add("5230432");
        normalFinish.add("5230433");
        normalFinish.add("5230434");
        partsStatus.setNormalFinish(normalFinish);

        List<String> normalRun = new ArrayList<>(3);
        normalRun.add("5230435");
        normalRun.add("5230436");
        normalRun.add("5230437");
        partsStatus.setNormalRun(normalRun);

        List<String> delayFinish = new ArrayList<>(3);
        delayFinish.add("5230438");
        delayFinish.add("5230439");
        delayFinish.add("5230440");
        partsStatus.setDelayFinish(delayFinish);

        List<String> delayRun = new ArrayList<>(3);
        delayRun.add("5230441");
        delayRun.add("5230442");
        delayRun.add("5230443");
        partsStatus.setDelayRun(delayRun);

        return partsStatus;
    }
}
