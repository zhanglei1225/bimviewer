package com.shenzhong.bimviewer.service;

import com.alibaba.fastjson.JSONObject;
import com.shenzhong.bimviewer.bean.MyBim;
import com.shenzhong.bimviewer.bean.ViewToken;
import com.shenzhong.bimviewer.mapper.MyBimMapper;
import com.shenzhong.bimviewer.util.DateUtils;
import com.shenzhong.bimviewer.util.HttpClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class IndexService {
    private static final Logger logger = LoggerFactory.getLogger(IndexService.class);

    @Autowired
    MyBimMapper myBimMapper;

    public static Date accessTokenExpireDate; // accessToken凭证有效时间
    public static String myAccessToken; // accessToken保存凭证
    public static List<ViewToken> viewTokenList = new ArrayList<>(10);
    public static List<ViewToken> integrateViewTokenList = new ArrayList<>(5);

    /**
     * 获取访问token
     * @return
     */
    public String getAccessToken() {
        // 如果凭证有效直接返回凭证
        Date currentDate = new Date();
        if (accessTokenExpireDate != null && currentDate.before(accessTokenExpireDate)) {
            return myAccessToken;
        }

        String accessToken = HttpClientUtils.httpsRequestPOST("https://api.bimface.com/oauth2/token", "");
        JSONObject tokenJson = JSONObject.parseObject(accessToken);
        JSONObject dataJson = tokenJson.getJSONObject("data");
        String expireTime = dataJson.getString("expireTime");
        myAccessToken = dataJson.getString("token");
        logger.info("获取accessToken的数值为：" + myAccessToken);
        // 保存过期时间
        if (!StringUtils.isEmpty(expireTime)) {
            accessTokenExpireDate = DateUtils.parseDate(expireTime, DateUtils.YYYY_MM_DD_HH_MM_SS);
        }
        return myAccessToken;
    }

    /**
     * 获取视图token
     * @param fileId
     * @return
     */
    public String getViewToken(String fileId) {
        ViewToken currentViewToken = null;
        // 如果凭证有效直接返回凭证
        Date currentDate = new Date();
        for (ViewToken viewToken : viewTokenList) {
            // 比对fileId，不相同就返回
            if (!viewToken.getFileId().equals(fileId)) {
                continue;
            }

            if (viewToken.getExpireDate() != null && currentDate.before(viewToken.getExpireDate())) {
                return viewToken.getMyViewToken();
            } else {
                currentViewToken = viewToken;
            }
        }

        // 获取accessToken
        getAccessToken();
        String viewTokenJson = HttpClientUtils.httpsRequestGET(null,"https://api.bimface.com/view/token?fileId=" + fileId);
        JSONObject tokenJson = JSONObject.parseObject(viewTokenJson);
        String myViewToken = tokenJson.getString("data");
        if (currentViewToken == null) {
            currentViewToken = new ViewToken();
            currentViewToken.setFileId(fileId);
            viewTokenList.add(currentViewToken);
        }
        // 保存viewToken
        currentViewToken.setMyViewToken(myViewToken);
        // 保存过期时间
        if (!StringUtils.isEmpty(myViewToken)) {
            currentViewToken.setExpireDate(org.apache.commons.lang3.time.DateUtils.addHours(currentDate, 12));
        }
        logger.info("获取viewToken的数值为：" + myViewToken);

        return myViewToken;
    }

    public String getViewTokenByIntegrateId(String integrateId) {
        ViewToken currentViewToken = null;
        // 如果凭证有效直接返回凭证
        Date currentDate = new Date();
        for (ViewToken viewToken : integrateViewTokenList) {
            // 比对integrateId，不相同就返回
            if (!viewToken.getIntegrateId().equals(integrateId)) {
                continue;
            }

            if (viewToken.getExpireDate() != null && currentDate.before(viewToken.getExpireDate())) {
                return viewToken.getMyViewToken();
            } else {
                currentViewToken = viewToken;
            }
        }

        // 获取accessToken
        getAccessToken();
        String viewTokenJson = HttpClientUtils.httpsRequestGET(null,"https://api.bimface.com/view/token?integrateId=" + integrateId);
        JSONObject tokenJson = JSONObject.parseObject(viewTokenJson);
        String myViewToken = tokenJson.getString("data");
        if (currentViewToken == null) {
            currentViewToken = new ViewToken();
            currentViewToken.setIntegrateId(integrateId);
            integrateViewTokenList.add(currentViewToken);
        }
        // 保存viewToken
        currentViewToken.setMyViewToken(myViewToken);
        // 保存过期时间
        if (!StringUtils.isEmpty(myViewToken)) {
            currentViewToken.setExpireDate(org.apache.commons.lang3.time.DateUtils.addHours(currentDate, 12));
        }
        logger.info("获取integrateViewToken的数值为：" + myViewToken);

        return myViewToken;
    }

    public MyBim getBim(String shortCode) {
        if (StringUtils.isEmpty(shortCode)) {
            logger.info("短码为空！");
            return null;
        }
        MyBim bim = myBimMapper.getBimById(shortCode);
        if (bim == null) {
            logger.info("短码不存在！");
            return null;
        }
        return bim;
    }
}
