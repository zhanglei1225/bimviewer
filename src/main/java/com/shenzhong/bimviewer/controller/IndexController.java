package com.shenzhong.bimviewer.controller;

import com.shenzhong.bimviewer.bean.Constants;
import com.shenzhong.bimviewer.bean.FinishStatus;
import com.shenzhong.bimviewer.bean.MyBim;
import com.shenzhong.bimviewer.service.IndexService;
import com.shenzhong.bimviewer.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class IndexController {
    @Autowired
    IndexService indexService;

    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("accessToken", indexService.getViewToken(indexService.getBim(Constants.SZ_SHORT_CODE).getFileId()));
        return "index";
    }

    @RequestMapping("/jiuzi")
    public String progress(Model model){
        MyBim bim = indexService.getBim("jiuzibridge2");
        String accessToken;
        if (!StringUtils.isEmpty(bim.getFileId())) {
            accessToken = indexService.getViewToken(bim.getFileId());
        } else {
            accessToken = indexService.getViewTokenByIntegrateId(bim.getIntegrateId());
        }
        model.addAttribute("accessToken", accessToken);
        return "jiuzi";
    }

    @RequestMapping("/{shortCode}")
    public String preview(Model model, @PathVariable String shortCode){
        MyBim bim = indexService.getBim(shortCode);
        String accessToken;
        if (!StringUtils.isEmpty(bim.getFileId())) {
            accessToken = indexService.getViewToken(bim.getFileId());
        } else {
            accessToken = indexService.getViewTokenByIntegrateId(bim.getIntegrateId());
        }
        model.addAttribute("accessToken", accessToken);
        return "index";
    }

    /**
     * 通过bim后台访问
     * @param model
     * @param fileId
     * @return
     */
    @RequestMapping("/bim/{fileId}")
    public String look(Model model, @PathVariable String fileId){
        String accessToken = "";
        if (!StringUtils.isEmpty(fileId)) {
            accessToken = indexService.getViewToken(fileId);
        }
        model.addAttribute("accessToken", accessToken);
        return "index";
    }

    /**
     * 获取访问token
     * @param fileId
     * @return
     */
    @RequestMapping("/getAccessToken")
    @CrossOrigin(methods = {RequestMethod.GET})
    @ResponseBody
    public R getAccessToken(@RequestParam(name = "fileId") String fileId){
        String accessToken = "";
        if (!StringUtils.isEmpty(fileId)) {
            accessToken = indexService.getViewToken(fileId);
        }
        return R.ok().put("accessToken", accessToken);
    }

    /**
     * 获取访问token
     * @param fileId
     * @return
     */
    @RequestMapping("/getAccessTokenByIntegrateId")
    @CrossOrigin(methods = {RequestMethod.GET})
    @ResponseBody
    public R getAccessTokenByIntegrateId(@RequestParam(name = "fileId") String fileId){
        String accessToken = "";
        if (!StringUtils.isEmpty(fileId)) {
            accessToken = indexService.getViewTokenByIntegrateId(fileId);
        }
        return R.ok().put("accessToken", accessToken);
    }

    @RequestMapping("test")
    public String test(Model model){
        model.addAttribute("accessToken", indexService.getViewToken(indexService.getBim(Constants.SZ_SHORT_CODE).getFileId()));
        return "test";
    }

    @RequestMapping("/getAccessTokenByProjectName")
    @CrossOrigin(methods = {RequestMethod.GET})
    @ResponseBody
    public R getAccessTokenByProjectName(@RequestParam(name = "projectName") String projectName){
        MyBim bim = indexService.getBim(projectName);
        String accessToken;
        if (!StringUtils.isEmpty(bim.getFileId())) {
            accessToken = indexService.getViewToken(bim.getFileId());
        } else {
            accessToken = indexService.getViewTokenByIntegrateId(bim.getIntegrateId());
        }
        return R.ok().put("accessToken", accessToken);
    }

    @RequestMapping("/getBuildingFinishParts")
    @CrossOrigin(methods = {RequestMethod.GET})
    @ResponseBody
    public R getBuildingFinishParts(@RequestParam(name = "projectName") String projectName){
        FinishStatus finishStatus = new FinishStatus();
        List<String> layers = new ArrayList<>(3);
        layers.add("有地下室基础 -5.40");
        layers.add("无地下室基础 -3.15");
        finishStatus.setLevelNames(layers);
        return R.ok().put("accessToken", finishStatus);
    }
}
