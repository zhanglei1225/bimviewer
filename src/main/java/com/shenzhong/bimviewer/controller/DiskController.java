package com.shenzhong.bimviewer.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.aliyun.oss.*;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.*;
import com.shenzhong.bimviewer.bean.*;
import com.shenzhong.bimviewer.pojo.BatchGenerateLinkRequest;
import com.shenzhong.bimviewer.pojo.BatchGenerateLinkResponse;
import com.shenzhong.bimviewer.pojo.ShortLink;
import com.shenzhong.bimviewer.service.DiskCalendarService;
import com.shenzhong.bimviewer.service.DiskContentsService;
import com.shenzhong.bimviewer.service.LinkDownloadService;
import com.shenzhong.bimviewer.service.ShortLinkService;
import com.shenzhong.bimviewer.util.AjaxJson;
import com.shenzhong.bimviewer.util.AliOSSUtils;
import com.shenzhong.bimviewer.util.PageUtils;
import com.shenzhong.bimviewer.util.R;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@RestController
@RequestMapping("disk")
public class DiskController {

    @Autowired
    DiskContentsService diskContentsService;
    @Resource
    private LinkDownloadService linkDownloadService;
    @Autowired
    DiskCalendarService diskCalendarService;
    @Resource
    private ShortLinkService shortLinkService;

    /**
     * 登录接口
     *
     * @param userName
     * @param password
     * @return
     */
    @RequestMapping("login")
    public R login(@RequestParam(name = "userName") String userName, @RequestParam(name = "password") String password) {
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
            return R.error("用户名或密码不能为空");
        }
        DiskUser user = diskContentsService.getUser(userName);
        if (user == null) {
            return R.error("用户名或密码错误");
        } else {
            if (user.getUserName().equals(userName) && DigestUtil.md5Hex(password).equals(user.getPassword())) {
                return R.ok();
            }
        }
        return R.error("用户名或密码错误");
    }

    /**
     * 获取登录用户信息接口
     *
     * @param userName
     * @return
     */
    @RequestMapping("getUserInfo")
    public R getUserInfo(@RequestParam(name = "userName") String userName) {
        if (StringUtils.isEmpty(userName)) {
            return R.error("用户名不能为空");
        }

        DiskUser user = diskContentsService.getUser(userName);
        return R.ok().put("userInfo", user);
    }

    /**
     * 根据类型获取网盘内容
     *
     * @param params
     * @return
     */
    @RequestMapping("getFileList")
    public R getFileList(@RequestParam Map<String, Object> params) {

        PageUtils page = diskContentsService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 获取网盘统计信息
     *
     * @param companyName
     * @return
     */
    @RequestMapping("getStatistics")
    public R getStatistics(@RequestParam(name = "companyName", required = false, defaultValue = "aset") String companyName) {
        FileTypeStatistics statistics = new FileTypeStatistics();
        List<DiskContents> contentsList = diskContentsService.list();
        for (DiskContents contents : contentsList) {
            if (contents.getType() == Constants.FILE_TYPE_PHOTO) {
                statistics.setPhotoNum(statistics.getPhotoNum() + 1);
            } else if (contents.getType() == Constants.FILE_TYPE_VIDEO) {
                statistics.setVideoNum(statistics.getVideoNum() + 1);
            } else {
                statistics.setFileNum(statistics.getFileNum() + 1);
            }
        }
        return R.ok().put("fileStatistics", statistics);
    }

    /**
     * 获取创建人列表
     *
     * @param type
     * @return
     */
    @RequestMapping("getSearchCondition")
    public R getSearchCondition(@RequestParam(name = "type", required = false, defaultValue = "0") int type) {
        List<DiskContents> contentsList = diskContentsService.getCreatorList(type);
        SearchCondition searchCondition = new SearchCondition();
        List<String> userList = new ArrayList<>(10);
        List<String> partList = new ArrayList<>(10);
        for (DiskContents diskContents : contentsList) {
            if (StringUtils.isNotBlank(diskContents.getCreateUser())
                    && !userList.contains(diskContents.getCreateUser())) {
                userList.add(diskContents.getCreateUser());
            } else if (StringUtils.isNotBlank(diskContents.getPartName())
                    && !partList.contains(diskContents.getPartName())) {
                partList.add(diskContents.getPartName());
            }
        }
        searchCondition.setUserList(userList);
        searchCondition.setPartList(partList);

        return R.ok().put("searchCondition", searchCondition);
    }

    /**
     * 批量保存
     */
    @RequestMapping("/batchSave")
    public R batchSave(@RequestParam(name = "createUser") String createUser, @RequestParam(name = "description", required = false) String description,
                       @RequestParam(name = "fileNames") String fileNames, @RequestParam(name = "fileUrl") String fileUrl,
                       @RequestParam(name = "partName") String partName, @RequestParam(name = "type") int type) {
        if (StringUtils.isBlank(fileNames)) {
            return R.error("文件名列表为空");
        }
        List<DiskContents> contentsList = new ArrayList<>(5);
        String[] files = fileNames.split(",");
        for (String fileName : files) {
            DiskContents diskContents = new DiskContents();
            diskContents.setType(type);
            diskContents.setCreateUser(createUser);
            diskContents.setDescription(description);
            diskContents.setFileName(fileName);
            diskContents.setFileUrl(fileUrl);
            diskContents.setPartName(partName);
            contentsList.add(diskContents);
        }

        diskContentsService.saveBatch(contentsList);
        // 保存图片上传日期
        diskCalendarService.saveUploadDate(type);
        return R.ok();
    }

    /**
     * 编辑文件
     */
    @PostMapping("/updateFile")
    public R updateFile(@RequestParam(name = "id") String id, @RequestParam(name = "description", required = false) String description,
                        @RequestParam(name = "fileName") String fileName, @RequestParam(name = "partName") String partName,
                        @RequestParam(name = "type") int type) {
        if (StringUtils.isBlank(id)) {
            return R.error("文件id为空");
        }
        DiskContents diskContents = new DiskContents();
        diskContents.setId(id);
        diskContents.setType(type);
        diskContents.setDescription(description);
        diskContents.setFileName(fileName);
        diskContents.setPartName(partName);

        diskContentsService.saveOrUpdate(diskContents);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestParam Long[] ids) {
        diskContentsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    private String endpoint = "http://oss-cn-beijing.aliyuncs.com";
    private String accessKeyId = "LTAI5tLERDJjXwEQ9xY7KVdK";
    private String accessKeySecret = "QQ6TcMJxeW5xbvjgEOxXvxeD7xFdsm";
    private String bucketName = "asetdisk";
    private String prefix ="aset/photo/";
    //oss存储课时文件前缀
    private String dir = "image/";
    private String dir1 = "/aset/photo/";

//    private String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
//    private String accessKeyId = "LTAI5tDqDosKz8dsHhsRuiaX";
//    private String accessKeySecret = "vuNml43TYGCZ9HbK3TjzZIKYCe4TrZ";
//    private String bucketName = "web-framework-itheima";
//    private String prefix ="picture/";

    /**
     * 上传文件
     *
     * @return
     */
    @GetMapping("getOSS")
    @ResponseBody
    public Map<String, Object> getOSS() {
        HashMap<String, Object> map1 = new HashMap<>();
        try {
            //开启oss客户端
            OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);


            //生成的到期时间转换位s，并转换为String
            String expire = String.valueOf(expireEndTime / 1000);
            PolicyConditions policyConditions = new PolicyConditions();

            //构造用户表单域Policy条件
            PolicyConditions policyConds = new PolicyConditions();
            //设置上传文件大小的范围
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            //设置上传的路径的前缀:就是上传到指定文件夹
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            //根据到期时间生成policy策略
            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            //对policy进行UTF-8编码后转base64
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String endPolicy = BinaryUtil.toBase64String(binaryData);
            //生成签名，policy表单域作为填入的值，将该值作为将要签名的字符串。
            String signature = ossClient.calculatePostSignature(postPolicy);

            //封装参数参数返回
            HashMap<String, Object> map = new HashMap<>();
            map.put("accessid", accessKeyId);
            map.put("host", endpoint);//这里我是直接传过去的，因为在前端访问oss的时候做了拼接的，可以在aliossUploader.js里面找到
            map.put("policy", endPolicy);
            map.put("signature", signature);
            map.put("expire", expire);
            map1.put("code", 0);
            map1.put("success", true);
            map1.put("msg", "签名成功");
            map1.put("data", map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map1;
    }

    @Autowired
    private AliOSSUtils aliOSSUtils;

    /**
     * 后台上传
     *
     * @param image
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    public R upload(MultipartFile image) throws IOException {
        //调用阿里云OSS工具类，将上传上来的文件存入阿里云
        String url = aliOSSUtils.upload(image);
        //将图片上传完成后的url返回，用于浏览器回显展示
        return R.ok(url);
    }

    /**
     * 生成链接
     *
     * @param batchGenerateLinkRequest
     * @return
     */
    @PostMapping("/api/short-link/batch/generate")
    @ResponseBody
    public AjaxJson<List<BatchGenerateLinkResponse>> generatorShortLink(@RequestBody @Valid BatchGenerateLinkRequest batchGenerateLinkRequest) {
        Long expireTime = batchGenerateLinkRequest.getExpireTime();
        List<BatchGenerateLinkResponse> result = new ArrayList<>();
        //文件访问路径
        //http://asetdisk.oss-cn-beijing.aliyuncs.com/aset/photo/%E4%B8%89%E7%BB%B408.png
        String storageId = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + dir1;
        for (String path : batchGenerateLinkRequest.getPaths()) {
            ShortLink shortLink = shortLinkService.generatorShortLink(storageId, path, expireTime);
            String shortUrl ="/s/" + shortLink.getShortKey();
            result.add(new BatchGenerateLinkResponse(shortUrl, ""));
        }
        return AjaxJson.getSuccessData(result);
    }

    /**
     * 跳转短链
     * @param key
     * @throws IOException
     */
    @GetMapping("/s/{key}")
    @ResponseStatus(HttpStatus.FOUND)
    @ApiOperation(value = "跳转短链", notes = "根据短链 key 跳转（302 重定向）到对应的直链.")
    @ApiImplicitParam(paramType = "path", name = "key", value = "短链 key", required = true, dataTypeClass = String.class)
    public void parseShortKey(@PathVariable String key) throws IOException {
        linkDownloadService.handlerShortLink(key);
    }


    /**
     * 分享链接
     *
     * @param batchGenerateLinkRequest
     * @return
     */
    @PostMapping("/short-link/batch/generate")
    @ResponseBody
    public AjaxJson<String> generatorShortLink1(@RequestBody @Valid BatchGenerateLinkRequest batchGenerateLinkRequest) {
        if (batchGenerateLinkRequest.getUploadDate().size()>0) {
          List<String> paths=  diskContentsService.getPathList(batchGenerateLinkRequest);
          batchGenerateLinkRequest.setPaths(paths);
        }
//        列表的大小不会小于 0 列表的大小只可能是等于 0 或大于 0
        if (batchGenerateLinkRequest.getPaths().isEmpty()) {
            return AjaxJson.getError("没有可供分享文件");
        }
        Long expireTime = batchGenerateLinkRequest.getExpireTime();
        String storageId = endpoint.split("//")[0] + "//" + bucketName + "." + endpoint.split("//")[1] + dir1;
        if (batchGenerateLinkRequest.getPaths().size() == 1) {
            String path = batchGenerateLinkRequest.getPaths().get(0);
            ShortLink shortLink = shortLinkService.generatorShortLink(storageId,path,expireTime);
            String shortUrl ="/s/" + shortLink.getShortKey();
            return AjaxJson.getSuccessData(shortUrl);
        }
        List<String> objectKeys = new ArrayList<>(); // 存储多个文件的对象键
        for (String path : batchGenerateLinkRequest.getPaths()) {
            objectKeys.add(prefix+path);
        }
        // 创建 OSS 客户端实例
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        String shareLink="";
        String zipObjectKey="";
        try {
            // 创建压缩文件流
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
            // 获取需要压缩的对象并添加到压缩文件中
            for (String objectKey : objectKeys) {
                // 创建GetObjectRequest对象
                GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, objectKey);
                // 获取对象
                OSSObject ossObject = null;
                try {
                    ossObject = ossClient.getObject(getObjectRequest);
                } catch (OSSException e) {
                    log.info("获取对象{}时发生异常：{} " ,objectKey,e.getMessage());
                    continue;
                }
                // 获取对象的元数据
                ObjectMetadata objectMetadata = ossObject.getObjectMetadata();
                ZipEntry zipEntry = new ZipEntry(objectKey);
                zipEntry.setSize(objectMetadata.getContentLength());
                zipOutputStream.putNextEntry(zipEntry);
                IOUtils.copy(ossClient.getObject(getObjectRequest).getObjectContent(), zipOutputStream);
                zipOutputStream.closeEntry();
            }

            // 完成压缩，关闭流
            zipOutputStream.finish();
            zipOutputStream.close();

            // 将压缩文件上传到 OSS
            byte[] zipBytes = outputStream.toByteArray();
            ByteArrayInputStream inputStream = new ByteArrayInputStream(zipBytes);
            zipObjectKey = prefix+System.currentTimeMillis()+".zip"; // 设置压缩文件的对象键
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(zipBytes.length);
            ossClient.putObject(bucketName, zipObjectKey, inputStream, metadata);

            // 生成分享链接
            shareLink = ossClient.generatePresignedUrl(bucketName, zipObjectKey, new Date(System.currentTimeMillis()+(expireTime*1000L))).toString();
            log.info("打包后的分享链接: {}",shareLink);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭 OSS 客户端
            ossClient.shutdown();
        }
        String path = zipObjectKey.split("/")[2];
        ShortLink shortLink = shortLinkService.generatorShortLink(storageId,path,expireTime);
        String shortUrl ="/s/" + shortLink.getShortKey();
        return AjaxJson.getSuccessData(shortUrl);
    }

    /**
     * 插入网盘图片上传日期
     */
    @RequestMapping("/uploadDateTest")
    public R uploadDateTest(@RequestParam String uploadDate) {
        diskCalendarService.saveCalendar(uploadDate);
        return R.ok();
    }

    /**
     * 查询有照片的日期
     */
    @RequestMapping("/searchUploadDate")
    public R searchUploadDate(@RequestParam String uploadDate) {
        if (uploadDate.length() != 4 && uploadDate.length() != 7) {
            R.error("查询日期格式错误");
        }
        List<DiskCalendar> calendarList = diskCalendarService.list();
        List<String> dateList = new ArrayList<>(5);
        // 查询年历
        if (uploadDate.length() == 4) {
            for (DiskCalendar calendar : calendarList) {
                if (calendar.getUploadDate().startsWith(uploadDate)) {
                    dateList.add(calendar.getUploadDate().substring(0, 7));
                }
            }
        } else if (uploadDate.length() == 7) { // 查询月历
            for (DiskCalendar calendar : calendarList) {
                if (calendar.getUploadDate().startsWith(uploadDate)) {
                    dateList.add(calendar.getUploadDate());
                }
            }
        }
        HashSet dateSet = new HashSet(dateList);
        List<String> photoList = new ArrayList<>(dateSet);
        return R.ok().put("photoList", photoList);
    }
}
