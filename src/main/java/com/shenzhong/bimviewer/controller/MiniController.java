package com.shenzhong.bimviewer.controller;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shenzhong.bimviewer.bean.BridgePart;
import com.shenzhong.bimviewer.bean.Constants;
import com.shenzhong.bimviewer.bean.MiniUser;
import com.shenzhong.bimviewer.bean.SMS;
import com.shenzhong.bimviewer.pojo.Items1;
import com.shenzhong.bimviewer.pojo.Items2;
import com.shenzhong.bimviewer.pojo.Items3;
import com.shenzhong.bimviewer.pojo.Root;
import com.shenzhong.bimviewer.service.BridgePartService;
import com.shenzhong.bimviewer.service.MiniService;
import com.shenzhong.bimviewer.service.SMSService;
import com.shenzhong.bimviewer.util.R;
import com.shenzhong.bimviewer.util.SendMessageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("mini")
public class MiniController {

    @Autowired
    MiniService miniService;

    @Autowired
    SMSService smsService;

    @Autowired
    BridgePartService bridgePartService;

    /**
     * 登录接口
     * @param userName
     * @param password
     * @return
     */
    @RequestMapping("login")
    public R login(@RequestParam(name = "userName") String userName, @RequestParam(name = "password") String password) {
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
            return R.error("用户名或密码不能为空");
        }
        MiniUser user = miniService.getUser(userName);
        if (user == null) {
            return R.error("用户名或密码错误");
        } else {
            if (user.getUserName().equals(userName) && DigestUtil.md5Hex(password).equals(user.getPassword())) {
                return R.ok();
            }
        }
        return R.error("用户名或密码错误");
    }

    /**
     * 验证码登录接口
     * @param phoneNum
     * @param code
     * @return
     */
    @RequestMapping("msgLogin")
    public R msgLogin(@RequestParam(name = "phoneNum") String phoneNum, @RequestParam(name = "code") String code) {
        if (StringUtils.isEmpty(phoneNum) || StringUtils.isEmpty(code)) {
            return R.error(Constants.FILED_BLANK, "手机号和验证码不能为空");
        }

        if (!PhoneUtil.isMobile(phoneNum)) {
            return R.error(Constants.PHONE_NUM_INVALID, "手机号非法");
        }

        if (miniService.isPhoneExists(phoneNum) <= 0) {
            return R.error(Constants.USER_NOT_EXIST, "不存在用户");
        }

        // 核实验证码
        QueryWrapper<SMS> wrapper = new QueryWrapper<SMS>()
                .eq("mobile", phoneNum).orderByDesc("create_time");
        List<SMS> smsList = smsService.list(wrapper);
        SMS sms = null;
        if (smsList.size() > 0) {
            sms = smsList.get(0);
        }
        if (sms == null) {
            return R.error(Constants.CODE_NOT_EXISTS, "请先获取验证码，再登录");
        } else if (!code.equals(sms.getCode())) {
            return R.error(Constants.CODE_ERROR, "验证码错误");
        } else if (sms.getInvalidTime().isBefore(LocalDateTime.now())) {
            return R.error(Constants.CODE_OUT_OF_DATE, "验证码过期，请重新获取");
        }

        return R.ok();
    }

    /**
     * 获取登录用户信息接口
     * @param userName
     * @return
     */
    @RequestMapping("getUserInfo")
    public R getUserInfo(@RequestParam(name = "userName") String userName) {
        if (StringUtils.isEmpty(userName)) {
            return R.error("用户名不能为空");
        }

        MiniUser user = miniService.getUser(userName);
        return R.ok().put("userInfo", user);
    }

    /**
     * 构建完成接口
     * @param partId
     * @param status
     * @return
     */
    @PostMapping("finish")
    public R finish(@RequestParam(name = "partId") String partId, @RequestParam(name = "status") Integer status) {
        if (StringUtils.isEmpty(partId)) {
            return R.error("构建ID不能为空");
        }

        boolean isSuccess = bridgePartService.setFinish(partId, status);
        if (!isSuccess) {
            return R.error("构建ID不存在");
        }

        return R.ok();
    }

    /**
     * 发送短信
     * @param phoneNum
     * @return
     */
    @PostMapping("sendMessage")
    public R sendMessage(@RequestParam(name = "phoneNum") String phoneNum) {
        if (StringUtils.isEmpty(phoneNum)) {
            return R.error(Constants.FILED_BLANK, "手机号不能为空");
        }

        if (!PhoneUtil.isMobile(phoneNum)) {
            return R.error(Constants.PHONE_NUM_INVALID, "手机号非法");
        }

        if (miniService.isPhoneExists(phoneNum) <= 0) {
            return R.error(Constants.USER_NOT_EXIST, "不存在用户");
        }

        String sendCode = SendMessageUtils.sendMessage(phoneNum);
        // 存储code到数据库
        saveSMS(phoneNum, sendCode);

        return R.ok();
    }

    private void saveSMS(String mobile, String sendCode) {
        SMS sms = new SMS();
        sms.setMobile(mobile);
        sms.setCode(sendCode);
        sms.setInvalidTime(LocalDateTime.now().plusMinutes(5));
        smsService.save(sms);
    }

    /**
     * 获取大桥结构信息接口
     * @param bridgeId
     * @return
     */
    @RequestMapping("getBridgeInfo")
    public R getBridgeInfo(@RequestParam(name = "bridgeId") String bridgeId) {

        return R.ok().put("bridgeInfo", bridgePartService.getBridgeInfo(bridgeId));
    }

    /**
     * 获取模型构建完成状态接口
     * @param bridgeId
     * @return
     */
    @RequestMapping("getFinishInfo")
    @CrossOrigin(methods = {RequestMethod.GET})
    public R getFinishInfo(@RequestParam(name = "bridgeId") String bridgeId) {
        return R.ok().put("finishInfo", bridgePartService.getFinishList(bridgeId));
    }

    /**
     * 获取全部模型构建，并存储到数据库
     * @return
     */
    @RequestMapping("saveModelParts")
    public R saveModelParts() {
        List<BridgePart> partList = new ArrayList<>(10);
        String projectId = "b_001";
        // 获取json数据，打开获取构建树bim测试文件，按f12获取
        String json = HttpUtil.get("https://m.bimface.com/17efb59ea4a5f66b67ada95dd6541f74/data/tree.json");
//        System.out.println(json);
        Root root = JSONUtil.toBean(json, Root.class);
        for (int i = 0; i < root.getData().size(); i++) {
            for (int j = 0; j < root.getData().get(i).getItems().size(); j++) {
                Items1 items1 = root.getData().get(i).getItems().get(j);
                for (Items2 items2 : items1.getItems()) {
                    for (Items3 items3 : items2.getItems()) {
                        System.out.println(items3.getName() + ", " + items3.getItems().get(0).getElementIds());
                        String[] names = items3.getName().split("_");
                        String[] descArr = {"上部结构", "下部结构", "附属设施"};
                        if (names.length >= 3 && ListUtil.toList(descArr).contains(names[0])) {
                            BridgePart bridgePart = new BridgePart();
                            bridgePart.setBridgeId(projectId);
                            bridgePart.setName(names[2]);
                            bridgePart.setTypeName(names[1]);
                            bridgePart.setPositionDesc(names[0]);
                            bridgePart.setIsFinish(0);
                            bridgePart.setPartId(items3.getItems().get(0).getElementIds().get(0));
                            partList.add(bridgePart);
                        }
                    }
                }
            }
        }
        bridgePartService.saveBatch(partList);

        return R.ok();
    }

    /**
     * 获取模型构建完成状态接口
     * @param projectName
     * @return
     */
    @RequestMapping("getPartsStatus")
    @CrossOrigin(methods = {RequestMethod.GET})
    public R getPartsStatus(@RequestParam(name = "projectName") String projectName) {

        return R.ok().put("partsStatus", bridgePartService.getPartsStatus());
    }
}
