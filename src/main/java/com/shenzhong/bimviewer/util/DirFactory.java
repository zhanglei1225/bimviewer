package com.shenzhong.bimviewer.util;

import com.shenzhong.bimviewer.bean.Constants;

/**
 * @author cwx
 * @create 2021-02-22 10:27 上午
 */
public class DirFactory {
    // Win系统
    public static String jksDir = Constants.win_path;

    static {
        setMacDir();
    }



    private static void setMacDir() {
        // mac系统
        if (OSInfoUtils.isMacOSX()) {
            String userHome = System.getProperties().getProperty("user.home");
            jksDir = userHome + Constants.macbook_path;
        } else if (OSInfoUtils.isLinux()) { // Linux系统
            jksDir = "/usr/local/dev/mixingstation/airbim.jks";
        }
    }


    public static void main(String[] args) {

    }
}
