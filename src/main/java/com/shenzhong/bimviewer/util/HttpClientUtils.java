package com.shenzhong.bimviewer.util;

import com.shenzhong.bimviewer.bean.Constants;
import com.shenzhong.bimviewer.service.IndexService;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * 功能：http请求工具类
 * date：2018/11/27 16:20
 * auther：denghaibo
 **/
public class HttpClientUtils {


    /**
     * 功能：get请求，返回请求的原始数值
     * @param para 参数
     * @param httpUrl 请求地址
     * @return
     */
    public static String httpsRequestGET(Map<String, String> para,String httpUrl){
        CloseableHttpClient client = HttpClients.createDefault();
        String buffer = null;
        try {
            URIBuilder builder = new URIBuilder(httpUrl);
            if(para!=null){
                Set<String> set = para.keySet();
                for(String key: set){
                    builder.setParameter(key, para.get(key));
                }
            }
            HttpGet httpGet = new HttpGet(builder.build());
            httpGet.addHeader("Content-Type", "text/xml;charset=UTF-8");
            httpGet.addHeader("Authorization", "Bearer " + IndexService.myAccessToken);
            CloseableHttpResponse response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            buffer = EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return buffer;
        }
    }

    /**
     * 功能：get请求，返回请求的原始数值
     * @param requestParams 参数
     * @param httpUrl 请求地址
     * @return
     */
    public static String httpsRequestPOST(String httpUrl,String requestParams ){
        CloseableHttpClient client = HttpClients.createDefault();
        String buffer = null;
        try {
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(300 * 1000).setConnectTimeout(300 * 1000).build();
            HttpPost httpPost = new HttpPost(httpUrl);
            httpPost.addHeader("Content-Type", "text/xml;charset=UTF-8");
            httpPost.addHeader("Authorization", Constants.AUTHORIZATION_CODE);
            StringEntity postingString = new StringEntity(requestParams, "utf-8");
            httpPost.setEntity(postingString);
            CloseableHttpResponse response = client.execute(httpPost);
            HttpEntity entity = response.getEntity();
            buffer = EntityUtils.toString(entity, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return buffer;
        }
    }


}
