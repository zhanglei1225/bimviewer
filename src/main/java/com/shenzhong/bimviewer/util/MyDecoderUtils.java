package com.shenzhong.bimviewer.util;

import java.util.Base64;

public class MyDecoderUtils {
    public static void main(String[] args) {
        try {
            final Base64.Decoder decoder = Base64.getDecoder();
            final Base64.Encoder encoder = Base64.getEncoder();
            final String text = "w8GE8iHkqzXiLNs9sVcY28lwdQne96dU:szr30TZssJhBVXG8sDK1FFRmzpiPVycD";
            final byte[] textByte = text.getBytes("UTF-8");
            //编码
            final String encodedText = encoder.encodeToString(textByte);
            System.out.println(encodedText);
            //解码
            System.out.println(new String(decoder.decode(encodedText), "UTF-8"));
        } catch (Exception e) {

        }
    }
}
