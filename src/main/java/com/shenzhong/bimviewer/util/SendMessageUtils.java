package com.shenzhong.bimviewer.util;

import cn.hutool.core.util.RandomUtil;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.shenzhong.bimviewer.bean.SMS;

import java.time.LocalDateTime;

public class SendMessageUtils {

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    public static void main(String[] args) {
        sendMessage("15056582503");
//        sendAll();
    }

    public static String sendMessage(String mobile) {
        String code = generateCode();
        try {
            com.aliyun.dysmsapi20170525.Client client;
            client = SendMessageUtils.createClient("LTAI5tLERDJjXwEQ9xY7KVdK", "QQ6TcMJxeW5xbvjgEOxXvxeD7xFdsm");
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setPhoneNumbers(mobile)
                    .setSignName("AirBIM形象进度")
                    .setTemplateCode("SMS_276075054")
                    .setTemplateParam("{\"code\":\"" + code + "\"}");
            // 复制代码运行请自行打印 API 的返回值
            SendSmsResponse sendSmsResponse = client.sendSms(sendSmsRequest);
//            System.out.println(sendSmsResponse.getBody().message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    public static String generateCode() {
        return RandomUtil.randomNumbers(6);
    }
}