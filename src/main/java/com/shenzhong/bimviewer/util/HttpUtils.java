package com.shenzhong.bimviewer.util;

import cn.hutool.http.HttpUtil;

import java.util.HashMap;

public class HttpUtils {
    public static void main(String[] args) {
        getFilePage();
    }

    public static void searchUploadDate() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("uploadDate", "2023-05");
        String result = HttpUtil.post("http://localhost:8080/preview/disk/searchUploadDate", paramMap);
//        String result = HttpUtil.post("http://192.168.0.110:8999/preview/disk/login", paramMap);
        System.out.println(result);
    }

    public static void uploadDate() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("uploadDate", "2023-05-08");
        String result = HttpUtil.post("http://localhost:8080/preview/disk/uploadDateTest", paramMap);
//        String result = HttpUtil.post("http://192.168.0.110:8999/preview/disk/login", paramMap);
        System.out.println(result);
    }

    public static void getFilePage() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("type", 0);
        paramMap.put("uploadDate", "2023-05");
        String result = HttpUtil.post("http://localhost:8080/preview/disk/getFileList", paramMap);
//        String result = HttpUtil.post("http://192.168.0.110:8999/preview/disk/login", paramMap);
        System.out.println(result);
    }

    public static void diskLogin() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("userName", "admin");
        paramMap.put("password", "shenzhong160616");
//        String result = HttpUtil.post("http://localhost:8080/preview/disk/login", paramMap);
        String result = HttpUtil.post("http://192.168.0.110:8999/preview/disk/login", paramMap);
        System.out.println(result);
    }

    public static void saveContents() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("createUser", "admin");
        paramMap.put("description", "描述");
        paramMap.put("fileNames", "aaa.jpg,bbb.jpg");
        paramMap.put("fileUrl", "aset/image");
        paramMap.put("partName", "上部结构");
        paramMap.put("type", 0);
        String result = HttpUtil.post("http://localhost:8080/preview/disk/batchSave", paramMap);
        System.out.println(result);
    }

    public static void cameraCtl() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("accessToken", "at.84wov3pu3l5vygnr1q6fxpbsdhm6ipan-2h7ojm05xz-0e9ol60-kpnmjbsfs");
        paramMap.put("deviceSerial", "G66144012");
        paramMap.put("channelNo", "1");
        paramMap.put("direction", "3");
        paramMap.put("speed", "1");
        String result = HttpUtil.post("https://open.ys7.com/api/lapp/device/ptz/start", paramMap);
        System.out.println(result);
    }

    public static void getContents() {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("type", 0);
        paramMap.put("page", 1);
        String result = HttpUtil.post("http://localhost:8080/preview/disk/getFileList", paramMap);
        System.out.println(result);
    }

    public static void delete() {
        HashMap<String, Object> paramMap = new HashMap<>();
        Long[] ids = new Long[1];
        ids[0] = 1L;
        paramMap.put("ids", ids);
        String result = HttpUtil.post("http://localhost:8080/preview/disk/delete", paramMap);
        System.out.println(result);
    }

    public static void sendSms() {
        // 转发到新服务器
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("phoneNum", "15056582503");
        // paramMap.put("code", "099823");
        String result = HttpUtil.post("http://localhost:8080/preview/mini/sendMessage", paramMap);
        // String result = HttpUtil.post("http://192.168.0.110:8999/preview/mini/sendMessage", paramMap);
        System.out.println(result);
    }

    public static void smsLogin() {
        // 转发到新服务器
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("phoneNum", "15056582503");
        paramMap.put("code", "736980");
        String result = HttpUtil.post("http://localhost:8080/preview/mini/msgLogin", paramMap);
        // String result = HttpUtil.post("http://192.168.0.110:8999/preview/mini/sendMessage", paramMap);
        System.out.println(result);
    }
}
