package com.shenzhong.bimviewer.bean;

import java.util.Date;

public class ViewToken {
    private String fileId; // 文件id
    private String integrateId; // 集成id
    private Date expireDate; // viewToken凭证有效时间
    private String myViewToken; // viewToken保存凭证

    public String getIntegrateId() {
        return integrateId;
    }

    public void setIntegrateId(String integrateId) {
        this.integrateId = integrateId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getMyViewToken() {
        return myViewToken;
    }

    public void setMyViewToken(String myViewToken) {
        this.myViewToken = myViewToken;
    }
}
