package com.shenzhong.bimviewer.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "bus_sms")
public class SMS {
    @TableId(value = "id",type = IdType.AUTO)
    private String id;
    private String userId;
    private String mobile;
    private String code;
    private Integer type;
    private LocalDateTime invalidTime;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
