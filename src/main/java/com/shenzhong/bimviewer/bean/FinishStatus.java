package com.shenzhong.bimviewer.bean;

import lombok.Data;

import java.util.List;

@Data
public class FinishStatus {
    List<String> levelNames;
    List<String> types;
    List<String> ids;
}
