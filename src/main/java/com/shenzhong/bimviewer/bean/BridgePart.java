package com.shenzhong.bimviewer.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "bus_bridge_part")
public class BridgePart {
    @TableId(value = "id",type = IdType.AUTO)
    private String id;

    private String bridgeId;

    private String name;

    private String typeName;

    private String positionDesc;

    private String partId;

    private int isFinish;

    @TableLogic
    private int isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
