package com.shenzhong.bimviewer.bean;

import java.util.Date;

public class MyBim {
    private String id;
    private String author;
    private String fileId;
    private String integrateId;
    private String desc;
    private Date createTime;

    public String getIntegrateId() {
        return integrateId;
    }

    public void setIntegrateId(String integrateId) {
        this.integrateId = integrateId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
