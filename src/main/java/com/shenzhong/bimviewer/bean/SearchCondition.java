package com.shenzhong.bimviewer.bean;

import lombok.Data;

import java.util.List;

@Data
public class SearchCondition {
    private List<String> userList;

    private List<String> partList;
}
