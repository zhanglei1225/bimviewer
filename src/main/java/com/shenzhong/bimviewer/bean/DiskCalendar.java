package com.shenzhong.bimviewer.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "disk_calendar")
public class DiskCalendar {
    @TableId(value = "id",type = IdType.AUTO)
    private String id;

    private String createUser;

    private String uploadDate;

    @TableLogic
    private int isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
