package com.shenzhong.bimviewer.bean;

import lombok.Data;

import java.util.List;

@Data
public class PartsStatus {
    List<String> normalFinish;
    List<String> delayFinish;
    List<String> normalRun;
    List<String> delayRun;
}
