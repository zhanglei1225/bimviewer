package com.shenzhong.bimviewer.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "disk_contents")
public class DiskContents {
    @TableId(value = "id",type = IdType.AUTO)
    private String id;

    private String description;

    private String createUser;

    private String partName;

    private String fileName;

    private String fileUrl;

    private String memo;

    private int type;

    @TableLogic
    private int isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
