package com.shenzhong.bimviewer.bean;

import lombok.Data;

// 网盘用户
@Data
public class DiskUser {
    // 用户名
    private String userName;

    // 密码
    private String password;

    // 项目名称
    private String projectName;

    // 角色
    private String role;

    // 手机号
    private String phone;
}
