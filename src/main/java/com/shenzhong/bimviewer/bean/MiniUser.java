package com.shenzhong.bimviewer.bean;

import lombok.Data;

@Data
public class MiniUser {
    private String userName;

    private String password;

    private String projectName;

    private String role;

    private String phone;
}
