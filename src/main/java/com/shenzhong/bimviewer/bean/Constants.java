package com.shenzhong.bimviewer.bean;

public class Constants {
    public static final String win_path = "D:/projects/aset/mixingstation/src/main/resources/airbim.jks";

    public static final String macbook_path = "/Documents/projects/aset/mixingstation/src/main/resources/airbim.jks";

    // 照片
    public static final int FILE_TYPE_PHOTO = 0;

    // 视频
    public static final int FILE_TYPE_VIDEO = 1;

    // 其他
    public static final int FILE_TYPE_OTHER = 2;

    // 授权码 Basic + Base64加密字符串
    public static final String AUTHORIZATION_CODE = "Basic dzhHRThpSGtxelhpTE5zOXNWY1kyOGx3ZFFuZTk2ZFU6c3pyMzBUWnNzSmhCVlhHOHNESzFGRlJtenBpUFZ5Y0Q=";

    // 公司模型短码
    public static final String SZ_SHORT_CODE = "nOE9zSeu";

    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     *  升序
     */
    public static final String ASC = "asc";

    // 手机号和验证码为空
    public static final int FILED_BLANK = 600;

    // 不存在用户
    public static final int USER_NOT_EXIST = 601;

    // 验证码错误
    public static final int CODE_ERROR = 602;

    // 手机号非法
    public static final int PHONE_NUM_INVALID = 603;

    // 验证码不存在
    public static final int CODE_NOT_EXISTS = 604;

    // 验证码过期
    public static final int CODE_OUT_OF_DATE = 605;
}
