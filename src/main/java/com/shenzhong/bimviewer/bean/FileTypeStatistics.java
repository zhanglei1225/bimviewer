package com.shenzhong.bimviewer.bean;

import lombok.Data;

@Data
public class FileTypeStatistics {
    private int photoNum;
    private int videoNum;
    private int fileNum;
}
