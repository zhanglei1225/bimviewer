package com.shenzhong.bimviewer;

import com.shenzhong.bimviewer.util.DirFactory;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan(value = "com.shenzhong.bimviewer.mapper")
public class BimViewerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BimViewerApplication.class, args);
	}

	/**
	 * 配置http / https 协议
	 */
	@Value("${https.port}")
	private Integer port;

	/*@Bean
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
		tomcat.addAdditionalTomcatConnectors(createSslConnector()); // 添加http
		return tomcat;
	}

	private Connector createSslConnector(){
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
		connector.setScheme("https");
		connector.setSecure(true);
		connector.setPort(port);
		protocol.setSSLEnabled(true);
		protocol.setKeystoreFile(DirFactory.jksDir);
		protocol.setKeystorePass("x0p9cer2");

		return connector;
	}*/

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BimViewerApplication.class);
	}

}
