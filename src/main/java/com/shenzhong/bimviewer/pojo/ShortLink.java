package com.shenzhong.bimviewer.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 短链信息 entity
 *
 * @author zhaojun
 */
@Data
@ApiModel(description = "短链信息")
@TableName(value = "short_link")
public class ShortLink implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    @ApiModelProperty(value = "ID, 新增无需填写")
    private String id;


    @TableField(value = "storage_id")
    @ApiModelProperty(value = "存储源 ID")
    private String storageId;


    @TableField(value = "short_key")
    @ApiModelProperty(value = "短链 key", example = "voldd3")
    private String shortKey;


    @TableField(value = "url")
    @ApiModelProperty(value = "短链 url", example = "/directlink/1/test02.png")
    private String url;


    @TableField(value = "create_date")
    @ApiModelProperty(value = "创建时间", example = "2021-11-22 10:05")
    private Date createDate;


    @TableField(value = "expire_date")
    @ApiModelProperty(value = "过期时间", example = "2021-11-23 10:05")
    private Date expireDate;


}
