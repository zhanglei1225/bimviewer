package com.shenzhong.bimviewer.pojo;

import java.util.List;

@lombok.Data
public class Root
{
    private String code;

    private String type;

    private List<Data> data;
}