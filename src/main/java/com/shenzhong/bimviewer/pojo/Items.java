package com.shenzhong.bimviewer.pojo;

import java.util.ArrayList;
import java.util.List;
public class Items
{
    private String fileId;

    private List<String> elementIds;

    public void setFileId(String fileId){
        this.fileId = fileId;
    }
    public String getFileId(){
        return this.fileId;
    }
    public void setElementIds(List<String> elementIds){
        this.elementIds = elementIds;
    }
    public List<String> getElementIds(){
        return this.elementIds;
    }
}