package com.shenzhong.bimviewer.pojo;

import java.util.ArrayList;
import java.util.List;
public class Items2
{
    private String type;

    private String id;

    private String name;

    private String actualName;

    private int elementCount;

    private List<Items3> items;

    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setActualName(String actualName){
        this.actualName = actualName;
    }
    public String getActualName(){
        return this.actualName;
    }
    public void setElementCount(int elementCount){
        this.elementCount = elementCount;
    }
    public int getElementCount(){
        return this.elementCount;
    }
    public void setItems(List<Items3> items){
        this.items = items;
    }
    public List<Items3> getItems(){
        return this.items;
    }
}